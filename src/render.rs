use crate::Palette;
use image::{
    imageops::{resize, Nearest},
    Rgb, RgbImage,
};

pub fn render_palette(pal: &Palette) -> RgbImage {
    let mut image = RgbImage::new(512, 256);
    let greyscale = pal.clone().greyscale();
    for (x, y, rgb) in image.enumerate_pixels_mut() {
        let index = (x / 64) as usize;
        *rgb = match y {
            0..=63 => pal[index],
            64..=127 => pal[index + 8],
            128..=191 => greyscale[index],
            192.. => greyscale[index + 8],
        };
    }
    image
}

pub fn render_screen(screen_bytes: &[u8], pal: &Palette) -> RgbImage {
    let mut image: RgbImage = RgbImage::new(320, 256);
    let mut pixels: Vec<&mut Rgb<u8>> = image.pixels_mut().into_iter().collect();

    let mut i = 0;
    let border_colour = pal[0];

    for _top_border in 0..(32 * 320) {
        *pixels[i] = border_colour;
        i += 1;
    }

    for third in 0..3 {
        for row in 0..8 {
            for line in 0..8 {
                let mut attribs = 6144 + ((third * 8 + row) << 5);
                let mut bits = (third << 11) + (line << 8) + (row << 5);

                for _left_border_width in 0..32 {
                    *pixels[i] = border_colour;
                    i += 1;
                }

                for _screen_line in 0..32 {
                    let attr = screen_bytes[attribs];
                    let mut bitmask = screen_bytes[bits];
                    attribs += 1;
                    bits += 1;

                    let bright = attr & 0b01000000 != 0;
                    let _flash = attr & 0b10000000 != 0;
                    let ink = attr & 0b00000111;
                    let paper = (attr & 0b00111000) >> 3;

                    for _bit_index in 0..8 {
                        let pixel = bitmask & 0b10000000 != 0;
                        let colour = if pixel { ink } else { paper } as usize;
                        bitmask <<= 1;
                        if bright {
                            *pixels[i] = pal[colour + 8];
                        } else {
                            *pixels[i] = pal[colour];
                        }
                        i += 1;
                    }
                }

                for _right_border_width in (32 + 256)..320 {
                    *pixels[i] = border_colour;
                    i += 1;
                }
            }
        }
    }

    for _bottom_border in 0..(32 * 320) {
        *pixels[i] = border_colour;
        i += 1;
    }

    resize(&image, 640, 512, Nearest)
}
