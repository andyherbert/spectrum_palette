mod linear_rgb;
mod oklab;

use image::Rgb;
use linear_rgb::LinearRgb;
use oklab::OkLab;

impl From<LinearRgb> for Rgb<u8> {
    fn from(rgb: LinearRgb) -> Rgb<u8> {
        fn transfer_function(value: f64) -> f64 {
            if value >= 0.0031308 {
                1.055 * value.powf(1.0 / 2.4) - 0.055
            } else {
                12.92 * value
            }
        }
        let r = (transfer_function(rgb.r) * 255.0).round() as u8;
        let g = (transfer_function(rgb.g) * 255.0).round() as u8;
        let b = (transfer_function(rgb.b) * 255.0).round() as u8;
        Rgb([r, g, b])
    }
}

impl From<OkLab> for Rgb<u8> {
    fn from(lab: OkLab) -> Rgb<u8> {
        Rgb::from(LinearRgb::from(lab))
    }
}

#[derive(Clone, Debug, PartialEq, Eq)]
pub struct Palette {
    rgb: Vec<Rgb<u8>>,
}

impl Palette {
    pub fn new() -> Palette {
        let rgb = vec![
            Rgb([0x00, 0x00, 0x00]),
            Rgb([0x00, 0x00, 0xff]),
            Rgb([0xff, 0x00, 0x00]),
            Rgb([0xff, 0x00, 0xff]),
            Rgb([0x00, 0xff, 0x00]),
            Rgb([0x00, 0xff, 0xff]),
            Rgb([0xff, 0xff, 0x00]),
            Rgb([0xff, 0xff, 0xff]),
        ];
        Palette { rgb }
    }

    pub fn clamp_rgb(mut self, value: u8) -> Palette {
        for mut rgb in self.rgb.iter_mut().skip(1) {
            rgb.0[0] = rgb.0[0].min(value);
            rgb.0[1] = rgb.0[1].min(value);
            rgb.0[2] = rgb.0[2].min(value);
        }
        self
    }

    pub fn multiply_lightness(mut self, value: f64) -> Palette {
        for rgb in self.rgb.iter_mut().skip(1) {
            let mut lab = OkLab::from(*rgb);
            lab.l *= value;
            *rgb = Rgb::from(lab);
        }
        self
    }

    pub fn greyscale(mut self) -> Palette {
        for rgb in self.rgb.iter_mut() {
            let mut lab = OkLab::from(*rgb);
            lab.a = 0.0;
            lab.b = 0.0;
            *rgb = Rgb::from(lab);
        }
        self
    }

    pub fn spread_lightness_with_shift(mut self, value: f64) -> Palette {
        for (i, rgb) in self.rgb.iter_mut().enumerate() {
            let mut lab = OkLab::from(*rgb);
            lab.l = value + ((1.0 - value) / 7.0 * i as f64);
            *rgb = Rgb::from(lab);
        }
        self
    }

    pub fn spread_lightness(self) -> Palette {
        self.spread_lightness_with_shift(0.0)
    }

    pub fn extend(mut self, other: Palette) -> Palette {
        self.rgb.extend(other.rgb);
        self
    }
}

use std::ops::{Index, IndexMut};

impl Index<usize> for Palette {
    type Output = Rgb<u8>;

    fn index(&self, index: usize) -> &Self::Output {
        &self.rgb[index]
    }
}

impl IndexMut<usize> for Palette {
    fn index_mut(&mut self, index: usize) -> &mut Self::Output {
        &mut self.rgb[index]
    }
}

use std::fmt::{self, Display, Formatter};

impl Display for Palette {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        for rgb in &self.rgb {
            let value = (255 << 24)
                + ((rgb.0[2] as usize) << 16)
                + ((rgb.0[1] as usize) << 8)
                + rgb.0[0] as usize;
            writeln!(f, "{:#08x},", value)?;
        }
        Ok(())
    }
}
