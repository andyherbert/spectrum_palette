use super::{OkLab, Rgb};

#[derive(Clone, Copy, Debug, PartialEq)]
pub struct LinearRgb {
    pub r: f64,
    pub g: f64,
    pub b: f64,
}

impl From<OkLab> for LinearRgb {
    fn from(lab: OkLab) -> LinearRgb {
        let l = lab.l + 0.3963377774 * lab.a + 0.2158037573 * lab.b;
        let m = lab.l - 0.1055613458 * lab.a - 0.0638541728 * lab.b;
        let s = lab.l - 0.0894841775 * lab.a - 1.2914855480 * lab.b;
        let l = l.powf(3.0);
        let m = m.powf(3.0);
        let s = s.powf(3.0);
        LinearRgb {
            r: 4.0767416621 * l - 3.3077115913 * m + 0.2309699292 * s,
            g: -1.2684380046 * l + 2.6097574011 * m - 0.3413193965 * s,
            b: -0.0041960863 * l - 0.7034186147 * m + 1.7076147010 * s,
        }
    }
}

impl From<Rgb<u8>> for LinearRgb {
    fn from(rgb: Rgb<u8>) -> LinearRgb {
        fn linear_transform(value: f64) -> f64 {
            if value >= 0.04045 {
                ((value + 0.055) / (1.0 + 0.055)).powf(2.4)
            } else {
                value / 12.92
            }
        }
        let r = linear_transform(rgb.0[0] as f64 / 255.0);
        let g = linear_transform(rgb.0[1] as f64 / 255.0);
        let b = linear_transform(rgb.0[2] as f64 / 255.0);
        LinearRgb { r, g, b }
    }
}
