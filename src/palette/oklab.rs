use super::LinearRgb;
use image::Rgb;

#[derive(Clone, Copy, Debug, PartialEq)]
pub struct OkLab {
    pub l: f64,
    pub a: f64,
    pub b: f64,
}

impl From<LinearRgb> for OkLab {
    fn from(rgb: LinearRgb) -> OkLab {
        let l = 0.4122214708 * rgb.r + 0.5363325363 * rgb.g + 0.0514459929 * rgb.b;
        let m = 0.2119034982 * rgb.r + 0.6806995451 * rgb.g + 0.1073969566 * rgb.b;
        let s = 0.0883024619 * rgb.r + 0.2817188376 * rgb.g + 0.6299787005 * rgb.b;
        let l = l.cbrt();
        let m = m.cbrt();
        let s = s.cbrt();
        OkLab {
            l: 0.2104542553 * l + 0.7936177850 * m - 0.0040720468 * s,
            a: 1.9779984951 * l - 2.4285922050 * m + 0.4505937099 * s,
            b: 0.0259040371 * l + 0.7827717662 * m - 0.8086757660 * s,
        }
    }
}

impl From<Rgb<u8>> for OkLab {
    fn from(rgb: Rgb<u8>) -> OkLab {
        OkLab::from(LinearRgb::from(rgb))
    }
}

#[cfg(test)]
mod test {
    use super::{OkLab, Rgb};

    #[test]
    fn test_all_colours() {
        for blue in 0..=255 {
            for green in 0..=255 {
                for red in 0..=255 {
                    let rgb_1 = Rgb([red, green, blue]);
                    let lab = OkLab::from(rgb_1);
                    let rgb_2 = Rgb::from(lab);
                    assert_eq!(rgb_1, rgb_2);
                }
            }
        }
    }
}
