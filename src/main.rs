mod palette;
mod render;

use palette::Palette;
use render::{render_palette, render_screen};

const REDUCED_VOLTAGE: f64 = 0.846;
const BLACK_SHIFT: f64 = 0.14;

fn render_palettes() {
    let pal = Palette::new().clamp_rgb(0xee);
    let bright = Palette::new();
    let image = render_palette(&pal.extend(bright));
    image.save("png/wikipedia.png").unwrap();

    let pal = Palette::new().clamp_rgb(0xc0);
    let bright = Palette::new();
    let image = render_palette(&pal.extend(bright));
    image.save("png/emulators.png").unwrap();

    let pal = Palette::new().clamp_rgb(0xd8);
    let bright = Palette::new();
    let image = render_palette(&pal.extend(bright));
    image.save("png/reduced.png").unwrap();

    let pal = Palette::new()
        .spread_lightness()
        .multiply_lightness(REDUCED_VOLTAGE);
    let bright = Palette::new().spread_lightness();
    let image = render_palette(&pal.extend(bright));
    image.save("png/equalised.png").unwrap();

    let pal = Palette::new()
        .spread_lightness_with_shift(BLACK_SHIFT)
        .multiply_lightness(REDUCED_VOLTAGE);
    let bright = Palette::new().spread_lightness_with_shift(BLACK_SHIFT);
    let image = render_palette(&pal.extend(bright));
    image.save("png/equalised_tweaked.png").unwrap();
}

use std::{
    fs::{read_dir, File},
    io::Read,
};

fn main() {
    render_palettes();
    let pal = Palette::new()
        .spread_lightness_with_shift(BLACK_SHIFT)
        .multiply_lightness(REDUCED_VOLTAGE);
    let bright = Palette::new().spread_lightness_with_shift(BLACK_SHIFT);
    let pal = pal.extend(bright);
    for path in read_dir("scr").unwrap().flatten() {
        let mut file = File::open(path.path()).unwrap();
        let mut screen_bytes: Vec<u8> = Vec::with_capacity(6912);
        file.read_to_end(&mut screen_bytes).unwrap();
        if let Some(filename) = path.file_name().to_str() {
            let image = render_screen(&screen_bytes, &pal);
            image.save(format!("png/{filename}.png")).unwrap();
        }
    }
}
